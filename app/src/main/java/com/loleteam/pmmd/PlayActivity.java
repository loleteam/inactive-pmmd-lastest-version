package com.loleteam.pmmd;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.john.waveview.WaveView;

import java.util.Random;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener {

    private int firstNum = 0, secondNum = 0, ans = 0, score = -1;
    private Random rand = new Random();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        getSupportActionBar().hide();

        setInitialScene();

    }

    private void setInitialScene() {

        Button plus = (Button)findViewById(R.id.plus);
        Button minus = (Button)findViewById(R.id.minus);
        Button multiply = (Button)findViewById(R.id.multiply);
        Button divide = (Button)findViewById(R.id.divide);

        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        multiply.setOnClickListener(this);
        divide.setOnClickListener(this);

        generateQuesiton();


    }


    @Override
    public void onClick(View v) {
        Button onPressed = (Button)findViewById(v.getId());
        checkAns(onPressed.getText());
    }

    public void generateQuesiton() {

        int operator = rand.nextInt(4);
        score++;
        if(operator == 0) {
            firstNum = rand.nextInt(500) + 1;
            secondNum = rand.nextInt(300) + 1;
            ans = firstNum + secondNum;
            swapNum(firstNum, secondNum);


        }

        else if(operator == 1) {
            firstNum = rand.nextInt(999) + 1;
            secondNum = rand.nextInt(500) + 1;
            swapNum(firstNum, secondNum);
            ans = firstNum - secondNum;
        }

        else if(operator == 2) {
            firstNum = rand.nextInt(50) + 1;
            secondNum = rand.nextInt(40) + 1;
            ans = firstNum * secondNum;
            swapNum(firstNum, secondNum);
        }

        else if(operator == 3) {
            ans = rand.nextInt(50) + 1;
            secondNum = rand.nextInt(40) + 1;
            firstNum = ans * secondNum;
            swapNum(firstNum, secondNum);

        }

        TextView tv = (TextView)findViewById(R.id.question);
        tv.setText(firstNum + "_" + secondNum + " = " + ans);
        tv = (TextView)findViewById(R.id.score);
        tv.setText(String.valueOf(score));
       // animStopWatch();
        waveStopWatch();

    }

    public void checkAns(CharSequence operator) {


        if(operator.equals("+") && (firstNum + secondNum) == ans) {
            generateQuesiton();
        }

        else if(operator.equals("-") && (firstNum - secondNum) == ans) {
            generateQuesiton();
        }

        else if(operator.equals("*") && (firstNum * secondNum) == ans) {
            generateQuesiton();
        }

        else if(operator.equals("/") && (firstNum / secondNum) == ans) {
            generateQuesiton();
        }
        else gameOver();
    }

    public void gameOver() {
        Intent intent = new Intent(PlayActivity.this, ScoreActivity.class);
        intent.putExtra("score", score);
        startActivity(intent);
    }
/*
    public void animStopWatch() {
        ImageView bgStopWatch = (ImageView) findViewById(R.id.bgStopWatch);
        Animation countDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.stop_watch);
        bgStopWatch.startAnimation(countDown);

        countDown.setAnimationListener(new Animation.AnimationListener() {
                                           @Override
                                           public void onAnimationStart(Animation animation) {

                                           }

                                           @Override
                                           public void onAnimationEnd(Animation animation) {
                                               ImageView timeUpImg = (ImageView)findViewById(R.id.timeUpImg);
                                               timeUpImg.setImageResource(R.drawable.time_up_img);
                                               Handler handler = new Handler();
                                               handler.postDelayed(new Runnable() {
                                                   public void run() {
                                                       gameOver();
                                                   }
                                               }, 3000);
                                           }

                                           @Override
                                           public void onAnimationRepeat(Animation animation) {

                                           }
                                       });
        }
*/
        @Override
        public void onBackPressed() {
        }


        public void waveStopWatch() {
            final WaveView wave = (WaveView) findViewById(R.id.wave_view);
            final ObjectAnimator animation = ObjectAnimator.ofInt(wave, "progress", 95);
            animation.setDuration(3000);
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();

        }

        public void swapNum(int firstNum, int secondNum) {
            int tmpNum = 0;
            if(firstNum < secondNum) {
                firstNum = tmpNum;
                firstNum = secondNum;
                secondNum = tmpNum;
            }
        }

    }



