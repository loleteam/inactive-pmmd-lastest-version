package com.loleteam.pmmd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        Intent intent = getIntent();
        int score = intent.getExtras().getInt("score");

        TextView tv = (TextView)findViewById(R.id.score);
        tv.setText(String.valueOf(score));

        backToFuture();

    }

    public void backToFuture() {
        Button playBtn = (Button) findViewById(R.id.play_again_button);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScoreActivity.this, PlayActivity.class));
            }
        });
        Button mainMenu = (Button) findViewById(R.id.main_menu);
        mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScoreActivity.this, MainActivity.class));
            }
        });
    }
}
