package com.loleteam.pmmd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.john.waveview.WaveView;
import com.race604.drawable.wave.WaveDrawable;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        playGame();

        ImageView mImageView = (ImageView) findViewById(R.id.image);
        final WaveDrawable mWaveDrawable = new WaveDrawable(this, R.drawable.p);
        mImageView.setImageDrawable(mWaveDrawable);
        mWaveDrawable.setWaveAmplitude(5);
        mWaveDrawable.setWaveLength(200);
        mWaveDrawable.setWaveSpeed(5);
        mWaveDrawable.setLevel(6000);

    }

    public void playGame() {
        Button playBtn = (Button) findViewById(R.id.play_button);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PlayActivity.class));
            }
        });
    }

    private static class SimpleOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener{

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // Nothing
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // Nothing
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // Nothing
        }
    }

}
